#!/bin/bash
#Do these steps manually
#Either scp the script to the VM
#scp -r netvm_setup.sh user@vm_ip:~/
#Note you may need to first install:
#sudo apt-get install ssh
#sudo apt-get install dos2unix

#OR clone the setup script and copy to base folder

#sudo apt-get install git
#git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/cloudlab_startup.git
#cp cloudlab_startup/netvm_setup.sh .
#chmod +x netvm_setup.sh
#uptill here manually

#Other References 
#https://github.com/sdnfv/openNetVM/blob/master/docs/Install.md
#http://dpdk.org/dev
#https://gist.github.com/ConradIrwin/9077440


#Automated setup
#user setup
# Act as root (assume password less sudo (doesn't work)
#sudo -i
#Add user 'mininet' and assign default password 'mininet'
setup_user() {
    if ! id -u mininet > /dev/null 2>&1; then
    sudo useradd -m mininet
    fi
    echo mininet:mininet | sudo chpasswd
    sudo chown -hR mininet /home/mininet
    
    #Setup passwords for local users
    echo root:mininet|sudo chpasswd
    
    #Edit user previliges to allow passwordless sudo acces
    sudo sh -c "echo 'mininet ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers"
}
setup_user

configure_sshd() {
    #Edit the SSHD and restart service to enable ETHERNET Tunnels
    sudo sh -c "echo 'PermitTunnel yes' >> /etc/ssh/sshd_config"
    sudo sh -c "echo 'PasswordAuthentication yes' >> /etc/ssh/sshd_config"
    sudo service ssh restart
    sudo sysctl -w net.ipv4.tcp_ecn=1
}
configure_sshd

#configure for silent updates
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -q

sudo sysctl -p

#Install DPDK and OpenNetVM Dependcies
sudo apt-get -yq install build-essential linux-headers-$(uname -r) git make gcc
git clone https://github.com/sdnfv/openNetVM

#Store the current directory
parent_dir=`pwd`

install_opennetvm() {
    #Install DPDK
    cd openNetVM
    onvm_dir=`pwd`
    echo export onvm_dir=$onvm_dir >> ~/.bashrc
    git submodule init && git submodule update
    cd dpdk
    dpdk_dir=`pwd`
    echo export dpdk_dir=$dpdk_dir >> ~/.bashrc
    #setup RTE Params
    echo export RTE_SDK=$(pwd) >> ~/.bashrc
    echo export RTE_TARGET=x86_64-native-linuxapp-gcc  >> ~/.bashrc
    echo export ONVM_NUM_HUGEPAGES=1024 >> ~/.bashrc
    echo export ONVM_HOME=$onvm_dir >> ~/.bashrc
    #Ensure they are setup
    source ~/.bashrc
    sudo sh -c "echo 0 > /proc/sys/kernel/randomize_va_space"
    cd $onvm_dir
    cd scripts
    ./install.sh
   
    # cd $dpdk_dir/tools
    # ./dpdk-setup.sh 
    #choose options as and perform setup (usuually 13,16, 19 or 20, 22 (NIC info)),  

    # Debug and Explicitly Install the kernel modules
    sudo modprobe uio
    sudo insmod $dpdk_dir/build/kmod/igb_uio.ko

    # Debug and Configure the hugepages
    echo 1024 | sudo tee /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
    sudo mkdir /mnt/huge
    sudo mount -t hugetlbfs nodev /mnt/huge

}
install_opennetvm

test_dpdk() {
    #Trying Simple DPDK Examples
    #sudo ./build/helloworld -l 0,1 -n 1
    #sudo ./build/l2fwd -c f -n 4 -- -q 4 -p 3

    #Bind NICs to DPDK
    # cd $dpdk_dir/tools
    # ./dpdk-devnbind.py -s
    # ./dpdk-setup.sh 
    # ./dpdk-devnbind.py -b igb_uio 00:08.0
}
test_dpdk

test_opennetvm() {
    #Trying OpenetVM ( need Two terminals)
    #On Terminal 1
    #cd $onvm_dir/onvm
    #make clean && make ( might need to change Makefile in subfolders to avoid strict aliasing)
    #Add "CFLAGS += -fno-strict-aliasing"
    #./go.sh 0,1,2,3 3

    #On Terminal 2
    #cd $onvm_dir/examples/speed_tester
    #make clean && make ( might need to change Makefile to avoid strict aliasing)
    #Add "CFLAGS += -fno-strict-aliasing"
    #./go.sh 3 1 1
}
test_opennetvm