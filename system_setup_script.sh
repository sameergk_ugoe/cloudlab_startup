export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -q
sudo apt-get update

# Install git
sudo apt-get install git

#Install curl and wget
sudo apt-get install curl
sudo apt-get install wget

# Install Python Dev Tools
sudo apt-get install python-pip python-dev build-essential

# Intall Scapy
sudo apt-get install scapy

# Upgrade PIP and Install netifaces
sudo pip install --upgrade pip 
sudo pip install --upgrade virtualenv 
sudo pip install netifaces

# Install maptplotlib numpy
sudo pip install numpy
sudo pip install scipy
sudo pip install matplotlib

# Downlaod and Install D-ITG 
sudo wget http://traffic.comics.unina.it/software/ITG/codice/D-ITG-2.8.1-r1023-src.zip
sudo unzip D-ITG-2.8.1-r1023-src.zip
cd D-ITG-2.8.1-r1023/src
sudo make clean all
sudo make install PREFIX=/usr/local
cd ../../


#Download and Install Mininet and OpenvSwitch
sudo git clone git://github.com/mininet/mininet
cd mininet
sudo git tag
sudo git checkout -b 2.2.1 2.2.1
cd ..
sudo chmod +x -R *.sh *.py
sudo mininet/util/install.sh -a
sudo mn --test pingall > mn_pingout.txt 2>&1

#Downlaod and Install OpenvSwitch from GIT Repo

#Dowload and SYNC DRECN code from GIT Repo
sudo git clone https://sameergk_ugoe@bitbucket.org/sameergk_ugoe/drench.git
cd drench
sudo chmod +x -R *.sh *.py
cd ..
if [ ! -d "/home/mininet" ]; then
sudo mkdir /home/mininet
fi
sudo cp -r drench /home/mininet/



